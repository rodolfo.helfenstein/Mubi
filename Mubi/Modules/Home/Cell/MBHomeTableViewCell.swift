//
//  MBHomeTableViewCell.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 12/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher

class MBHomeTableViewCell : UITableViewCell {
    
    @IBOutlet weak var bannerImageView: AnimatedImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        
    }
    
    func setItem(show: MBShow) {
    
        self.titleLabel.text = show.title.lowercased()
        
        if let bannerImage = show.bannerImage {
            self.bannerImageView.kf.setImage(with: URL(string: bannerImage))
        }
        
    }
    
}
