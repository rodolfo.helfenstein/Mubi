//
//  MBSeason.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 12/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation
import SwiftyJSON

class MBSeason : NSObject, NSCoding {
    
    var id: Int
    var number: Int
    var title: String
    var bio: String
    var rating: Double
    var episodesCount: Int
    
    init(id: Int,
         number: Int,
         title: String,
         bio: String,
         rating: Double,
         episodesCount: Int) {
        self.id = id
        self.number = number
        self.title = title
        self.bio = bio
        self.rating = rating
        self.episodesCount = episodesCount
    }
    
    init(json : JSON){
        self.id = json["ids"]["trakt"].intValue
        self.number = json["number"].intValue
        self.title = json["title"].stringValue
        self.bio = json["overview"].stringValue
        self.rating = json["rating"].doubleValue
        self.episodesCount = json["episode_count"].intValue
    }
    
    required convenience init?(coder decoder: NSCoder) {
        
        guard
            let title = decoder.decodeObject(forKey: "title") as? String,
            let bio = decoder.decodeObject(forKey: "bio") as? String
            else { return nil }
            
        let id = decoder.decodeInteger(forKey: "id")
        let number = decoder.decodeInteger(forKey: "number")
        let rating = decoder.decodeDouble(forKey: "rating")
        let episodesCount = decoder.decodeInteger(forKey: "episodesCount")
        
        self.init(id: id, number: number, title: title, bio: bio, rating: rating, episodesCount: episodesCount)
        
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.id, forKey: "id")
        coder.encode(self.number, forKey: "number")
        coder.encode(self.title, forKey: "title")
        coder.encode(self.bio, forKey: "bio")
        coder.encode(self.rating, forKey: "rating")
        coder.encode(self.episodesCount, forKey: "episodesCount")
    }
}
