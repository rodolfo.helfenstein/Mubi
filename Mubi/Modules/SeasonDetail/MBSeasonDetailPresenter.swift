//
//  MBSeasonDetailPresenter.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 14/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation
import Viperit

final class MBSeasonDetailPresenter: Presenter {
    
    var show: MBShow!
    var season: MBSeason!
    
    func retrieveSeasonId(season: MBSeason, show: MBShow) {
        self.show = show
        self.season = season
    }
    
    override func viewHasLoaded() {
        self.view.setupNavigation(show: self.show, season: self.season)
        self.interactor.getEpisodes(withShow: self.show, andSeason: self.season)
    }
    
    func retrieveEpisodesList(episodes: [MBEpisode]?) {
        
        guard let episodes = episodes else {
            return
        }
        
        self.view.retrieveEpisodesList(episodes: episodes)
    }
    
    func showEpisodeDetail(withEpisode: MBEpisode){
        self.router.showEpisodeDetail(season: self.season, show: self.show, episode: withEpisode)
    }
    
}


// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension MBSeasonDetailPresenter {
    var view: MBSeasonDetailViewInterface {
        return _view as! MBSeasonDetailViewInterface
    }
    var interactor: MBSeasonDetailInteractor {
        return _interactor as! MBSeasonDetailInteractor
    }
    var router: MBSeasonDetailRouter {
        return _router as! MBSeasonDetailRouter
    }
}
