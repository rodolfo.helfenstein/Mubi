//
//  MBSeasonTableViewCell.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 14/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher

class MBEpisodeTableViewCell : UITableViewCell {
    
    @IBOutlet weak var episodeNumber: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        
    }
    
    func setItem(episode: MBEpisode) {
        self.episodeNumber.text = "E" + String(format: "%02d - ", episode.number)
        self.titleLabel.text = episode.title
    }
    
}
