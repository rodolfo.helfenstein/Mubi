//
//  BaseService.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 10/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class BaseService {

    static let shared = BaseService()

    private init() {}
    
    
    func makePostRequestSepareted(withUrl url: String,
                                  parameters: [String:Any],
                                  headers: [String:String],
                                  completionHandler: @escaping (DefaultDataResponse) -> Void){
        
        let ulr =  NSURL(string: url)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(Endpoint.traktClientId, forHTTPHeaderField: "trakt-api-key")
        request.setValue( "2", forHTTPHeaderField: "trakt-api-version")
        
        
        if let decoded  = UserDefaults.standard.object(forKey: Endpoint.LocalStorage.auth) as? Data,
            let decodedAuth = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? MBAuthentication {
            request.setValue(decodedAuth.tokenType.capitalized + " " + decodedAuth.accessToken, forHTTPHeaderField: "Authorization")
        }
        
        let data = try! JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        if let json = json {
            print(json)
        }
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
        
        
        Alamofire.request(request as URLRequestConvertible)
            .response { response in
                
                completionHandler(response)
                
        }
        
        
    }
    
    func makePostRequest(withUrl url: String,
                         parameters: [String:Any],
                         headers: [String:String],
                         completionHandler: @escaping (DefaultDataResponse) -> Void){
    
        var appendHeaders = headers
        
        appendHeaders["Content-Type"] = "application/json"
        appendHeaders["trakt-api-version"] = "2"
        appendHeaders["trakt-api-key"] = Endpoint.traktClientId
        
        if let decoded  = UserDefaults.standard.object(forKey: Endpoint.LocalStorage.auth) as? Data,
            let decodedAuth = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? MBAuthentication {
            appendHeaders["Authorization"] = decodedAuth.tokenType + " " + decodedAuth.accessToken
        }
    
        Alamofire.request(url,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.default,
                          headers: appendHeaders).response { response in
                            
            completionHandler(response)
                            
        }
    }
    
    func makeGetRequest(withUrl url: String,
                         parameters: [String:Any],
                         headers: [String:String],
                         completionHandler: @escaping (DefaultDataResponse) -> Void){
        
        var appendHeaders = headers
        
        appendHeaders["Content-Type"] = "application/json"
        appendHeaders["trakt-api-version"] = "2"
        appendHeaders["trakt-api-key"] = Endpoint.traktClientId
        
        var appendUrl = url
        var appendParams : [String:Any] = [:]
        
        if (!parameters.isEmpty) {
            
            for param in parameters {
                if appendUrl.range(of: param.key) != nil{
                    appendUrl = appendUrl.replacingOccurrences(of: param.key, with: param.value as! String)
                } else {
                    appendParams[param.key] = param.value
                }
            }
            
        }

        if let decoded  = UserDefaults.standard.object(forKey: Endpoint.LocalStorage.auth) as? Data,
            let decodedAuth = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? MBAuthentication {
            appendHeaders["Authorization"] = decodedAuth.tokenType + " " + decodedAuth.accessToken
        }
        
        Alamofire.request(appendUrl,
                          method: .get,
                          parameters: appendParams,
                          encoding: URLEncoding.default,
                          headers: appendHeaders).response { response in
                            
                            completionHandler(response)
                            
        }
    }
    
    func getUrlImages(withId: Int,
                      group: DispatchGroup,
                      completionHandler: @escaping (NSDictionary?) -> Void) {
        
        
        let headers = ["Content-Type" : "application/json"]
        let parameters = ["api_key": Endpoint.fanartTvAPIKey]
        let url = Endpoint.fanartTv.endpointShowImage.replacingOccurrences(of: "{showId}", with: "\(withId)")

        Alamofire.request(url,
                          method: .get,
                          parameters: parameters,
                          encoding: URLEncoding.default,
                          headers: headers).response { response in
                            
                            let imagesUrl = NSMutableDictionary()
                            let json = JSON(data: response.data!)
                            
                            if (json != JSON.null) {
                                imagesUrl.addEntries(from: ["bannerImage": json["tvbanner"][0]["url"].stringValue])
                                imagesUrl.addEntries(from: ["backgroundImage": json["showbackground"][0]["url"].stringValue])
                                 imagesUrl.addEntries(from: ["thumbImage": json["tvthumb"][0]["url"].stringValue])
                            }
                            
                            completionHandler(imagesUrl)
                            
                            group.leave()
                            
        }
    }
    
    func saveCache<T>(key: String,
                        withObject: T){
        
        let userDefaults = UserDefaults.standard
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: withObject)
        userDefaults.set(encodedData, forKey: key)
        userDefaults.synchronize()
        
    }
    
    func retriveCache<T>(key: String,
                                withObject: T.Type) -> T? {
        
        if let decoded  = UserDefaults.standard.object(forKey: key) as? Data,
            let decodedObject = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? T {
            return decodedObject
        }
        
        return nil
    }
}
