//
//  MBShowDetailInteractor.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 12/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation
import Viperit
import SwiftyJSON

final class MBShowDetailInteractor: Interactor {
    
    func getNextEpisode(withShow: MBShow) {
        
        let parameters = ["{showId}": withShow.slug,
                          "extended": "full"]
        
        BaseService.shared.makeGetRequest(withUrl: Endpoint.Episodes.nextEpisode, parameters: parameters, headers: [:]) { (response) in
            
            let json = JSON(data: response.data!)
            
            if (json != JSON.null) {
                
                let nextEpisode = MBEpisode(json: json)
                
                nextEpisode.releasedDate = json["first_aired"].stringValue
                
                BaseService.shared.saveCache(key: Endpoint.LocalStorage.nextEpisode + String(withShow.id), withObject: nextEpisode)
            }
            
            if let episode : MBEpisode = BaseService.shared.retriveCache(key: Endpoint.LocalStorage.nextEpisode + String(withShow.id), withObject: MBEpisode.self){
                
                self.presenter.retrieveNextEpisode(episode: episode)
            } else {
                self.presenter.retrieveNextEpisode(episode: nil)
            }
        }
        
    }

    func getSeasons(withName: String) {
    
        let parameters = ["{showName}": withName,
                          "extended": "full"]
        
        BaseService.shared.makeGetRequest(withUrl: Endpoint.Seasons.seasonList, parameters: parameters, headers: [:]) { (response) in
            
            let json = JSON(data: response.data!)
            var seasonList : [MBSeason] = []
                        
            if (json != JSON.null) {
                
                for value in json.arrayValue {
                    seasonList.append(MBSeason(json: value))
                }
                
                BaseService.shared.saveCache(key: Endpoint.LocalStorage.seasonShowList + withName, withObject: seasonList)
            }

            if let seasonList : [MBSeason] = BaseService.shared.retriveCache(key: Endpoint.LocalStorage.seasonShowList + withName, withObject: [MBSeason].self){
                self.presenter.retrieveSeasonList(seasonList: seasonList)
            } else {
                self.presenter.retrieveSeasonList(seasonList: nil)
            }
        }
    }
    
    func getProgress(withShow: MBShow){
        
        let parameters = ["{showId}": withShow.slug,
                          "extended": "full"]
        
        BaseService.shared.makeGetRequest(withUrl: Endpoint.Watching.progressList, parameters: parameters, headers: [:]) { (response) in
            
            let json = JSON(data: response.data!)
            
            if (json != JSON.null) {
                
                let valuePercent = Double(json["completed"].intValue * 100 / json["aired"].intValue) / 100.0
                
                BaseService.shared.saveCache(key: Endpoint.LocalStorage.progressShow + String(withShow.id), withObject: valuePercent)
            }
            
            
            if let valuePercent : Double = BaseService.shared.retriveCache(key: Endpoint.LocalStorage.progressShow + String(withShow.id), withObject: Double.self){
                self.presenter.retrieveProgress(progress: valuePercent)
            } else {
                self.presenter.retrieveProgress(progress: nil)
            }
            
        }
        
    }
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension MBShowDetailInteractor {
    var presenter: MBShowDetailPresenter {
        return _presenter as! MBShowDetailPresenter
    }
}
