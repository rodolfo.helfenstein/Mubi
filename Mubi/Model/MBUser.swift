//
//  MBUser.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 11/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation
import SwiftyJSON

class MBUser : NSObject, NSCoding {
    
    var username: String
    var name: String
    var joinedAt: String
    var location: String
    var about: String
    var gender: String
    var age: String
    var profileImage: String
    var isVip: Bool
    
    init(username: String,
         name: String,
         joinedAt: String,
         location: String,
         about: String,
         gender: String,
         age: String,
         profileImage: String,
         isVip: Bool) {
        
        self.username = username
        self.name = name
        self.joinedAt = joinedAt
        self.location = location
        self.about = about
        self.gender = gender
        self.age = age
        self.profileImage = profileImage
        self.isVip = isVip
        
    }
    
    init(json : JSON){
        self.username = json["username"].stringValue
        self.name = json["name"].stringValue
        self.joinedAt = json["joined_at"].stringValue
        self.location = json["location"].stringValue
        self.about = json["about"].stringValue
        self.gender = json["gender"].stringValue
        self.age = json["age"].stringValue
        self.profileImage = json["images"]["avatar"]["full"].stringValue
        self.isVip = json["vip"].boolValue
    }
    
    required convenience init?(coder decoder: NSCoder) {
        guard
            let username = decoder.decodeObject(forKey: "username") as? String,
            let name = decoder.decodeObject(forKey: "name") as? String,
            let joinedAt = decoder.decodeObject(forKey: "joinedAt") as? String,
            let location = decoder.decodeObject(forKey: "location") as? String,
            let about = decoder.decodeObject(forKey: "about") as? String,
            let gender = decoder.decodeObject(forKey: "gender") as? String,
            let age = decoder.decodeObject(forKey: "age") as? String,
            let profileImage = decoder.decodeObject(forKey: "profileImage") as? String
            else { return nil }
        
        let isVip = decoder.decodeBool(forKey: "isVip")
        
        self.init(username: username, name: name, joinedAt: joinedAt, location: location, about: about, gender: gender, age: age, profileImage: profileImage, isVip: isVip)
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.username, forKey: "username")
        coder.encode(self.name, forKey: "name")
        coder.encode(self.joinedAt, forKey: "joinedAt")
        coder.encode(self.location, forKey: "location")
        coder.encode(self.about, forKey: "about")
        coder.encode(self.gender, forKey: "gender")
        coder.encode(self.age, forKey: "age")
        coder.encode(self.profileImage, forKey: "profileImage")
        coder.encode(self.isVip, forKey: "isVip")
    }
}

