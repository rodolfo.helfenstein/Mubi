//
//  AppDelegate+URL.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 09/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import UIKit

extension AppDelegate {

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)
        let code = urlComponents?.queryItems?.filter({ $0.name == "code" }).first
        NotificationCenter.default.post(name: .authComplete, object: code?.value)
        
        return true
    }
    
}
