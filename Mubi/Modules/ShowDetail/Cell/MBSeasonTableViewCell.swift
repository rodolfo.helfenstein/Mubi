//
//  MBSeasonTableViewCell.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 14/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher

class MBSeasonTableViewCell : UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var episodesCountLabel: UILabel!
    
    override func awakeFromNib() {
        
    }
    
    func setItem(season: MBSeason) {
        
        self.titleLabel.text = season.title.lowercased()
        self.ratingLabel.text = "rate: " + String(format: "%.1f", season.rating)
        self.episodesCountLabel.text = String(season.episodesCount) + (season.episodesCount > 1 ? " episodes" : " episode")
        
    }
    
}

