//
//  MBWelcomePresenter.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 09/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation
import Viperit
import SafariServices

final class MBWelcomePresenter: Presenter {
    
    
    override func viewHasLoaded() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(MBWelcomePresenter.retriveAuthCode(sender:)),
                                               name: .authComplete,
                                               object: nil)
    }
    
    func openAuth(){
    
        let params = ["response_type" : "code",
                      "client_id" : Endpoint.traktClientId,
                      "redirect_uri" : Endpoint.traktRedirectURI]
        
        let requestURL = URL(string:"\(Endpoint.Auth.authorize)?\(params.stringFromHttpParameters())")!
        
        self.view.openSafariWebView(requestURL)
    }
    
    @objc func retriveAuthCode(sender: Notification){
        self.view.closeSafariWebView()
        self.view.showLoader()
        self.interactor.getToken(withCode: sender.object as! String)
    }
    
    func completeAuth(withAuth auth: MBAuthentication?){
        
        if (auth != nil) {
            self.router.showHome()
        }
        
    }
    
}


// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension MBWelcomePresenter {
    var view: MBWelcomeViewInterface {
        return _view as! MBWelcomeViewInterface
    }
    var interactor: MBWelcomeInteractor {
        return _interactor as! MBWelcomeInteractor
    }
    var router: MBWelcomeRouter {
        return _router as! MBWelcomeRouter
    }
}
