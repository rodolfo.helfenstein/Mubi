//
//  MBEpisodeDetailView.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 15/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import UIKit
import Viperit

//MARK: - Public Interface Protocol
protocol MBEpisodeDetailViewInterface {
    func retrieveData(withShow: MBShow, andSeason: MBSeason, andEpisode: MBEpisode)
    func showAlertChecked(checked: Bool)
}

//MARK: MBEpisodeDetail View
final class MBEpisodeDetailView: UserInterface {
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var showTitle: UILabel!
    @IBOutlet weak var seasonName: UILabel!
    @IBOutlet weak var episodeName: UILabel!
    @IBOutlet weak var episodeBio: UITextView!
    @IBOutlet weak var buttonCheckWatched: UIButton!
}

//MARK: - Public interface
extension MBEpisodeDetailView: MBEpisodeDetailViewInterface {
    
    func retrieveData(withShow: MBShow, andSeason: MBSeason, andEpisode: MBEpisode) {
     
        guard
            let backgroundImage = withShow.backgroundImage else {
                return
        }
        
        self.backgroundImage.kf.setImage(with: URL(string: backgroundImage))
        self.showTitle.text = withShow.title
        self.seasonName.text = "S" + String(format: "%02d", andSeason.number) + "E" + String(format: "%02d", andEpisode.number)
        self.episodeName.text = "\"" + andEpisode.title + "\""
        self.episodeBio.text = andEpisode.bio != "" ? "\"" + andEpisode.bio + "\"" : ""
    }
    
    func showAlertChecked(checked: Bool){
    
        
        if (checked) {
            let alert = UIAlertController(title: "Watched! :)", message: "You check that episode which checked.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
            self.buttonCheckWatched.isHidden = true
            
        } else {
            let alert = UIAlertController(title: "Really?!! :)", message: "Houston, we have a problem.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }

    }
    
    @IBAction func buttonCheckWatched(_ sender: UIButton) {
        self.presenter.checkEpisode()
    }
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension MBEpisodeDetailView {
    var presenter: MBEpisodeDetailPresenter {
        return _presenter as! MBEpisodeDetailPresenter
    }
    var displayData: MBEpisodeDetailDisplayData {
        return _displayData as! MBEpisodeDetailDisplayData
    }
}
