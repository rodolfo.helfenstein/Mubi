//
//  MBSeasonDetailView.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 14/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import UIKit
import Viperit
import Kingfisher

//MARK: - Public Interface Protocol
protocol MBSeasonDetailViewInterface {
    func setupNavigation(show: MBShow, season: MBSeason)
    func retrieveEpisodesList(episodes: [MBEpisode])
}

//MARK: MBSeasonDetail View
final class MBSeasonDetailView: UserInterface {
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    var episodes: [MBEpisode] = []
}

//MARK: - Public interface
extension MBSeasonDetailView: MBSeasonDetailViewInterface {
    
    func retrieveEpisodesList(episodes: [MBEpisode]) {
        self.episodes = episodes
        self.tableView.reloadData()
    }

    func setupNavigation(show: MBShow, season: MBSeason) {
        
        guard
            let backgroundImage = show.backgroundImage else { return }
        
        self.backgroundImageView.kf.setImage(with: URL(string: backgroundImage))
    }
}

extension MBSeasonDetailView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return episodes.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.episodeCell.identifier) as! MBEpisodeTableViewCell
        
        cell.setItem(episode: self.episodes[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.showEpisodeDetail(withEpisode: self.episodes[indexPath.row])
    }
    
    
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension MBSeasonDetailView {
    var presenter: MBSeasonDetailPresenter {
        return _presenter as! MBSeasonDetailPresenter
    }
    var displayData: MBSeasonDetailDisplayData {
        return _displayData as! MBSeasonDetailDisplayData
    }
}
