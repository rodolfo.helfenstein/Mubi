//
//  MBShowDetailPresenter.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 12/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation
import Viperit

final class MBShowDetailPresenter: Presenter {
    
    var showDetail : MBShow!
    
    func retrieveShowId(show: MBShow) {
        self.showDetail = show
        self.interactor.getSeasons(withName: show.slug)
        self.interactor.getNextEpisode(withShow: show)
        self.interactor.getProgress(withShow: show)
    }
    
    func retrieveSeasonList(seasonList: [MBSeason]?){
        
        guard let seasonList = seasonList else {
            return
        }
    
        self.view.retrieveShowSeasons(showSeasons: seasonList)
    }
    
    func showSeasonDetail(season: MBSeason) {
        self.router.showSeasonDetail(season: season, show: self.showDetail)
    }
    
    func retrieveNextEpisode(episode: MBEpisode?) {
        
        guard let episode = episode else {
            return
        }
        
        self.view.retrieveNextEpisode(episode: episode)
    
    }
    
    func retrieveProgress(progress: Double?) {
        
        guard let progress = progress else {
            return
        }
        
        self.view.retrieveShowPercent(percent: progress)
    }
    
    override func viewHasLoaded() {
        self.view.setupNavigation(show: self.showDetail)
    }
}


// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension MBShowDetailPresenter {
    var view: MBShowDetailViewInterface {
        return _view as! MBShowDetailViewInterface
    }
    var interactor: MBShowDetailInteractor {
        return _interactor as! MBShowDetailInteractor
    }
    var router: MBShowDetailRouter {
        return _router as! MBShowDetailRouter
    }
}
