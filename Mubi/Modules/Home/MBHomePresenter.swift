//
//  MBHomePresenter.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 10/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation
import Viperit

final class MBHomePresenter: Presenter {
    
    override func viewHasLoaded() {
        self.view.showLoader()
        self.interactor.getProfileSettings()
    }
    
    func retriveProfileSettings(profileSettings: MBProfileSettings?){
        self.getWatchingList()
    }
    
    func getWatchingList(){
        
        if let profileSettings = BaseService.shared.retriveCache(key: Endpoint.LocalStorage.profileSettings, withObject: MBProfileSettings.self) {
            self.interactor.getShowWatchingList(username: profileSettings.user.username)
        }

    }
    
    func retrieveWatchingList(watchingList: [MBShow]?) {
        
        self.view.hideLoader()
        
        if let watchingList = watchingList {
            self.view.refreshTableView(watchingList: watchingList)
        }
    }
    
    func showShowDetail(show: MBShow) {
        self.router.showShowDetail(show: show)
    }
    
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension MBHomePresenter {
    var view: MBHomeViewInterface {
        return _view as! MBHomeViewInterface
    }
    var interactor: MBHomeInteractor {
        return _interactor as! MBHomeInteractor
    }
    var router: MBHomeRouter {
        return _router as! MBHomeRouter
    }
}
