 //
//  MBHomeInteractor.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 10/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation
import Viperit
import SwiftyJSON

final class MBHomeInteractor: Interactor {
    
    func getProfileSettings() {
        
        BaseService.shared.makeGetRequest(withUrl: Endpoint.Settings.profileSettings, parameters: [:], headers: [:]) { (response) in
            
            let json = JSON(data: response.data!)
            
            if (json != JSON.null) {
                BaseService.shared.saveCache(key: Endpoint.LocalStorage.profileSettings, withObject: MBProfileSettings(json: json))
            }
            
            if let profileSettings : MBProfileSettings = BaseService.shared.retriveCache(key: Endpoint.LocalStorage.profileSettings, withObject: MBProfileSettings.self) {
                self.presenter.retriveProfileSettings(profileSettings: profileSettings)
            } else {
                self.presenter.retriveProfileSettings(profileSettings: nil)
            }
        }
    }
    
    func getShowWatchingList(username: String) {
    
        let parameters = ["{filter}" : "shows"]
        
        BaseService.shared.makeGetRequest(withUrl: Endpoint.Watching.userWatching, parameters: parameters, headers: [:]) { (response) in
            
            let json = JSON(data: response.data!)
            let dispatchGroup = DispatchGroup()
            var watchingList : [MBShow] = []
            
            if (json != JSON.null) {
                
                for value in json.arrayValue {
                    
                    dispatchGroup.enter()
                    
                    BaseService.shared.getUrlImages(withId: value["show"]["ids"]["tvdb"].intValue, group: dispatchGroup) { (imagesUrl) in
                        
                        let show = MBShow(json: value)
                        
                        show.bannerImage = imagesUrl?.object(forKey: "bannerImage") as? String
                        show.backgroundImage = imagesUrl?.object(forKey: "backgroundImage") as? String
                        show.thumbImage = imagesUrl?.object(forKey: "thumbImage") as? String
                        watchingList.append(show)
                    }
                }
            }
            
            dispatchGroup.notify(queue: .main, execute: {
                
                if (json != JSON.null){
                    BaseService.shared.saveCache(key: Endpoint.LocalStorage.watchingShowList, withObject: watchingList)

                }
            
                if let watchingList : [MBShow] = BaseService.shared.retriveCache(key: Endpoint.LocalStorage.watchingShowList, withObject: [MBShow].self) {
                    self.presenter.retrieveWatchingList(watchingList: watchingList)
                } else {
                    self.presenter.retrieveWatchingList(watchingList: nil)
                }
            })
        }
    
    }
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension MBHomeInteractor {
    var presenter: MBHomePresenter {
        return _presenter as! MBHomePresenter
    }
    var presenterWelcome: MBWelcomePresenter {
        return _presenter as! MBWelcomePresenter
    }
}
