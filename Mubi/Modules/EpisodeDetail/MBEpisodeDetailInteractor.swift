//
//  MBEpisodeDetailInteractor.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 15/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import UIKit
import Viperit
import SwiftyJSON
import Alamofire

final class MBEpisodeDetailInteractor: Interactor {
 
    func checkEpisodeWatched(episode: MBEpisode, show: MBShow, season: MBSeason){
        
        
        let parameters: [String: Any] = [
            "shows": [
                [
                    "ids" : [
                        "trakt": show.id
                    ],
                    "seasons" : [
                        [
                            "number": season.number,
                            
                            "episodes": [
                                [
                                    "number": episode.number
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
        
        BaseService.shared.makePostRequestSepareted(withUrl: Endpoint.Episodes.checkWatched, parameters: parameters, headers: [:]) { (response) in
            
            let json = JSON(data: response.data!)
            
            if (json != JSON.null) {
                
                if (json["added"]["episodes"].intValue >= 1) {
                    self.presenter.retrieveCheckedEpisode(bool: true)
                } else {
                    self.presenter.retrieveCheckedEpisode(bool: false)
                }
            }
            
            self.presenter.retrieveCheckedEpisode(bool: false)
        }
    }
    
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension MBEpisodeDetailInteractor {
    var presenter: MBEpisodeDetailPresenter {
        return _presenter as! MBEpisodeDetailPresenter
    }
}
