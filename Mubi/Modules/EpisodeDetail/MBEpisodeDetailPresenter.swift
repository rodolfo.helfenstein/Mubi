//
//  MBEpisodeDetailPresenter.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 15/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation
import Viperit

final class MBEpisodeDetailPresenter: Presenter {
    
    var show: MBShow!
    var season: MBSeason!
    var episode: MBEpisode!
    
    func retrieveData(season: MBSeason, show: MBShow, episode: MBEpisode) {
        self.show = show
        self.season = season
        self.episode = episode
    }
    
    override func viewHasLoaded() {
        self.view.retrieveData(withShow: self.show, andSeason: self.season, andEpisode: self.episode)
    }
    
    func retrieveCheckedEpisode(bool: Bool) {
        self.view.showAlertChecked(checked: bool)
    }
    
    func checkEpisode(){
        self.interactor.checkEpisodeWatched(episode: self.episode, show: self.show, season: self.season)
    }
    
}


// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension MBEpisodeDetailPresenter {
    var view: MBEpisodeDetailViewInterface {
        return _view as! MBEpisodeDetailViewInterface
    }
    var interactor: MBEpisodeDetailInteractor {
        return _interactor as! MBEpisodeDetailInteractor
    }
    var router: MBEpisodeDetailRouter {
        return _router as! MBEpisodeDetailRouter
    }
}
