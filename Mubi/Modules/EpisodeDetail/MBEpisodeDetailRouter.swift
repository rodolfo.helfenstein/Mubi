//
//  MBEpisodeDetailRouter.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 15/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation
import Viperit

final class MBEpisodeDetailRouter: Router {
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension MBEpisodeDetailRouter {
    var presenter: MBEpisodeDetailPresenter {
        return _presenter as! MBEpisodeDetailPresenter
    }
}
