//
//  MBWelcomeInteractor.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 09/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation
import Viperit
import SwiftyJSON

final class MBWelcomeInteractor: Interactor {
    
    
    func getToken(withCode code: String) {
    
        let parameters = ["code" : code,
                          "client_id" : Endpoint.traktClientId,
                          "client_secret" : Endpoint.traktClientSecret,
                          "redirect_uri" : Endpoint.traktRedirectURI,
                          "grant_type" : "authorization_code"]
        
        BaseService.shared.makePostRequest(withUrl: Endpoint.Auth.token, parameters: parameters, headers: [:]) { (response) in

            let json = JSON(data: response.data!)
            
            if (json != JSON.null) {
                BaseService.shared.saveCache(key: Endpoint.LocalStorage.auth, withObject: MBAuthentication(json: json))
            }

            if let auth : MBAuthentication = BaseService.shared.retriveCache(key: Endpoint.LocalStorage.auth, withObject: MBAuthentication.self) {
                self.presenter.completeAuth(withAuth: auth)
            } else {
                self.presenter.completeAuth(withAuth: nil)
            }
        }
    
    }
    
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension MBWelcomeInteractor {
    var presenter: MBWelcomePresenter {
        return _presenter as! MBWelcomePresenter
    }
}
