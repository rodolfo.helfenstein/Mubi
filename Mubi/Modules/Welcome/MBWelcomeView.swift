//
//  MBWelcomeView.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 09/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import UIKit
import Viperit
import SafariServices
import PKHUD

//MARK: - Public Interface Protocol
protocol MBWelcomeViewInterface {
    func openSafariWebView(_ url: URL)
    func closeSafariWebView()
    func showLoader()
    func hideLoader()
}

//MARK: MBWelcome View
final class MBWelcomeView: UserInterface {
    
    var safariViewController : SFSafariViewController! = nil
    
    @IBAction func buttonSignInTouchedUpInside(_ sender: Any) {
        presenter.openAuth()
    }
    
}


extension MBWelcomeView: SFSafariViewControllerDelegate {

    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }

}

//MARK: - Public interface
extension MBWelcomeView: MBWelcomeViewInterface {
    
    func hideLoader() {
        HUD.hide()
    }

    func showLoader() {
        HUD.show(.progress)
    }


    func closeSafariWebView() {
        self.safariViewControllerDidFinish(self.safariViewController)
    }

    func openSafariWebView(_ url: URL){
        
        self.safariViewController = SFSafariViewController(url: url)
        self.present(self.safariViewController, animated: true, completion: nil)
        self.safariViewController.delegate = self
    }
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension MBWelcomeView {
    var presenter: MBWelcomePresenter {
        return _presenter as! MBWelcomePresenter
    }
    var displayData: MBWelcomeDisplayData {
        return _displayData as! MBWelcomeDisplayData
    }
}
