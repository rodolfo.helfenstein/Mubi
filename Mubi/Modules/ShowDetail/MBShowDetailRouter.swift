//
//  MBShowDetailRouter.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 12/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation
import Viperit

final class MBShowDetailRouter: Router {
    
    func showSeasonDetail(season: MBSeason, show: MBShow) {
        let module = Module.build(AppModules.MBSeasonDetail)
        (module.presenter as! MBSeasonDetailPresenter).retrieveSeasonId(season: season, show: show)
        module.router.show(from: _view)
    }
    
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension MBShowDetailRouter {
    var presenter: MBShowDetailPresenter {
        return _presenter as! MBShowDetailPresenter
    }
}
