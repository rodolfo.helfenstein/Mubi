//
//  MBSeasonDetailInteractor.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 14/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation
import Viperit
import SwiftyJSON

final class MBSeasonDetailInteractor: Interactor {
    
    func getEpisodes(withShow: MBShow, andSeason: MBSeason) {
        
        let parameters = ["{showId}": withShow.slug,
                          "{seasonId}": String(andSeason.number),
                          "translations": "en",
                          "extended": "full"]
        
        BaseService.shared.makeGetRequest(withUrl: Endpoint.Episodes.episodesList, parameters: parameters, headers: [:]) { (response) in
            
            let json = JSON(data: response.data!)
            var episodeList : [MBEpisode] = []
            
            if (json != JSON.null) {
                
                for value in json.arrayValue {
                    episodeList.append(MBEpisode(json: value))
                }
                
                BaseService.shared.saveCache(key: Endpoint.LocalStorage.episodesList + String(withShow.id) + String(andSeason.number), withObject: episodeList)
            }
            
            if let episodeList : [MBEpisode] = BaseService.shared.retriveCache(key: Endpoint.LocalStorage.episodesList + String(withShow.id) + String(andSeason.number), withObject: [MBEpisode].self){
                self.presenter.retrieveEpisodesList(episodes: episodeList)
            } else {
                self.presenter.retrieveEpisodesList(episodes: nil)
            }
        }
    }
    
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension MBSeasonDetailInteractor {
    var presenter: MBSeasonDetailPresenter {
        return _presenter as! MBSeasonDetailPresenter
    }
}
