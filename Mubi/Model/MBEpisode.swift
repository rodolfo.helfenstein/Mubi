//
//  MBEpisode.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 11/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation
import SwiftyJSON

class MBEpisode : NSObject, NSCoding {
    
    var id: Int
    var number: Int
    var title: String
    var bio: String
    var releasedDate: String?
    
    init(id: Int,
         number: Int,
         title: String,
         bio: String) {
        self.id = id
        self.number = number
        self.title = title
        self.bio = bio
    }
    
    init(id: Int,
         number: Int,
         title: String,
         bio: String,
         releasedDate: String) {
        self.id = id
        self.number = number
        self.title = title
        self.bio = bio
        self.releasedDate = releasedDate
    }
    
    init(json : JSON){
        self.id = json["ids"]["trakt"].intValue
        self.number = json["number"].intValue
        self.title = json["title"].stringValue
        self.bio = json["overview"].stringValue
    }

    
    required convenience init?(coder decoder: NSCoder) {
        
        guard
            let title = decoder.decodeObject(forKey: "title") as? String,
            let bio = decoder.decodeObject(forKey: "bio") as? String
            else { return nil }
        
        let id = decoder.decodeInteger(forKey: "id")
        let number = decoder.decodeInteger(forKey: "number")
        
        if let releasedDate = decoder.decodeObject(forKey: "releasedDate") as? String {
            self.init(id: id, number: number, title: title, bio: bio, releasedDate: releasedDate)
        } else {
            self.init(id: id, number: number, title: title, bio: bio)
        }
        
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.id, forKey: "id")
        coder.encode(self.number, forKey: "number")
        coder.encode(self.title, forKey: "title")
        coder.encode(self.bio, forKey: "bio")
        coder.encode(self.releasedDate, forKey: "releasedDate")
    }
}
