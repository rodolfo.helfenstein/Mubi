//
//  MBShowDetailView.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 12/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import UIKit
import Viperit
import Kingfisher

//MARK: - Public Interface Protocol
protocol MBShowDetailViewInterface {
    func setupNavigation(show: MBShow)
    func retrieveShowSeasons(showSeasons: [MBSeason])
    func retrieveNextEpisode(episode: MBEpisode)
    func retrieveShowPercent(percent: Double)
}

//MARK: MBShowDetail View
final class MBShowDetailView: UserInterface {
    
    @IBOutlet weak var backgroundImageView: AnimatedImageView!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var showThumb: AnimatedImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextEpisodeTitle: UILabel!
    @IBOutlet weak var nextEpisodeDate: UILabel!
    @IBOutlet weak var showTitle: UILabel!
    
    var showSeasons: [MBSeason] = []

}

//MARK: - Public interface
extension MBShowDetailView: MBShowDetailViewInterface {
    
    func retrieveShowPercent(percent: Double) {
        self.progressBar.progress = Float(percent)
    }

    func setupNavigation(show: MBShow) {
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.showThumb.layer.cornerRadius = self.view!.frame.width * 0.4 / 2
        
        guard
            let backgroundImage = show.backgroundImage,
            let thumbImage = show.thumbImage else {
            return
        }
        
        self.backgroundImageView.kf.setImage(with: URL(string: backgroundImage))
        
        self.showThumb.kf.indicator?.startAnimatingView()
        self.showThumb.kf.setImage(with: URL(string: thumbImage), placeholder: nil, options: nil, progressBlock: nil) { (image, error, cache, url) in
            self.showThumb.kf.indicator?.stopAnimatingView()
        }
        
        
        self.showTitle.text = show.title
    }
    
    func retrieveShowSeasons(showSeasons: [MBSeason]){
        self.showSeasons = showSeasons
        self.tableView.reloadData()
    }
    
    func retrieveNextEpisode(episode: MBEpisode) {
        self.nextEpisodeTitle.text = "E" + String(format: "%02d - ", episode.number) + episode.title
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let dateString = dateFormatter.string(from:(episode.releasedDate?.dateFromISO8601)!)
        
        self.nextEpisodeDate.text = dateString
        
    }
    
}

extension MBShowDetailView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return showSeasons.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.seasonCell.identifier) as! MBSeasonTableViewCell
        
        cell.setItem(season: self.showSeasons[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.showSeasonDetail(season: self.showSeasons[indexPath.row])
    }


}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension MBShowDetailView {
    var presenter: MBShowDetailPresenter {
        return _presenter as! MBShowDetailPresenter
    }
    var displayData: MBShowDetailDisplayData {
        return _displayData as! MBShowDetailDisplayData
    }
}
