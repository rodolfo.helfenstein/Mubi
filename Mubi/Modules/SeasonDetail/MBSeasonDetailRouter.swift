//
//  MBSeasonDetailRouter.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 14/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation
import Viperit

final class MBSeasonDetailRouter: Router {
    
    func showEpisodeDetail(season: MBSeason, show: MBShow, episode: MBEpisode) {
        let module = Module.build(AppModules.MBEpisodeDetail)
        (module.presenter as! MBEpisodeDetailPresenter).retrieveData(season: season, show: show, episode: episode)
        module.router.show(from: _view)
    }
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension MBSeasonDetailRouter {
    var presenter: MBSeasonDetailPresenter {
        return _presenter as! MBSeasonDetailPresenter
    }
}
