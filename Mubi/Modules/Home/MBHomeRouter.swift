//
//  MBHomeRouter.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 10/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation
import Viperit

final class MBHomeRouter: Router {
    
    func showShowDetail(show: MBShow) {
        let module = Module.build(AppModules.MBShowDetail)
        (module.presenter as! MBShowDetailPresenter).retrieveShowId(show: show)
        module.router.show(from: _view)
    }
    
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension MBHomeRouter {
    var presenter: MBHomePresenter {
        return _presenter as! MBHomePresenter
    }
}
