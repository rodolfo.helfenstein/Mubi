//
//  MBProfileSettings.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 11/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation
import SwiftyJSON

class MBProfileSettings : NSObject, NSCoding {
    
    var user : MBUser
    
    init(user: MBUser) {
        self.user = user
    }
    
    init(json : JSON){
        self.user = MBUser(json: json["user"])
    }
    
    required convenience init?(coder decoder: NSCoder) {
        guard let user = decoder.decodeObject(forKey: "user") as? MBUser
            else { return nil }
        
        self.init(user: user)
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.user, forKey: "user")
    }
}

