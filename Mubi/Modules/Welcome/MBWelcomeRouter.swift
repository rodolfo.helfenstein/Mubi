//
//  MBWelcomeRouter.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 09/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation
import Viperit

final class MBWelcomeRouter: Router {
    
    func showHome() {
        let module = Module.build(AppModules.MBHome)
        module.router.show(from: _view, embedInNavController: true, setupData: nil)
    }
    
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension MBWelcomeRouter {
    var presenter: MBWelcomePresenter {
        return _presenter as! MBWelcomePresenter
    }
}
