//
//  Notification+Name.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 09/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let authComplete = Notification.Name("authComplete")
}
