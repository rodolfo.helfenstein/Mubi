//
//  MBAuthentication.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 10/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation
import SwiftyJSON

class MBAuthentication : NSObject, NSCoding {
    
    var accessToken : String
    var expiresIn : String
    var refreshToken : String
    var scope : String
    var tokenType : String
    var createdAt : Int
    
    init(accessToken: String,
         expiresIn: String,
         refreshToken: String,
         scope: String,
         tokenType: String,
         createdAt: Int) {
    
        self.accessToken = accessToken
        self.expiresIn = expiresIn
        self.refreshToken = refreshToken
        self.scope = scope
        self.tokenType = tokenType
        self.createdAt = createdAt
        
    }
    
    init(json : JSON){
        self.accessToken = json["access_token"].stringValue
        self.expiresIn = json["expires_in"].stringValue
        self.refreshToken = json["refresh_token"].stringValue
        self.scope = json["scope"].stringValue
        self.tokenType = json["token_type"].stringValue
        self.createdAt = json["created_at"].intValue
    }
    
    required convenience init?(coder decoder: NSCoder) {
        guard let accessToken = decoder.decodeObject(forKey: "accessToken") as? String,
            let expiresIn = decoder.decodeObject(forKey: "expiresIn") as? String,
            let refreshToken = decoder.decodeObject(forKey: "refreshToken") as? String,
            let scope = decoder.decodeObject(forKey: "scope") as? String,
            let tokenType = decoder.decodeObject(forKey: "tokenType") as? String
            else { return nil }
        
            let createdAt = decoder.decodeInteger(forKey: "createdAt")
        
        self.init(accessToken: accessToken, expiresIn: expiresIn, refreshToken: refreshToken, scope: scope, tokenType: tokenType, createdAt: createdAt)
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.accessToken, forKey: "accessToken")
        coder.encode(self.expiresIn, forKey: "expiresIn")
        coder.encode(self.refreshToken, forKey: "refreshToken")
        coder.encode(self.scope, forKey: "scope")
        coder.encode(self.tokenType, forKey: "tokenType")
        coder.encode(self.createdAt, forKey: "createdAt")
    }
}
