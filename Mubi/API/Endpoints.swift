//
//  Endpoints.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 10/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

struct Endpoint {
    
    static let traktClientId = "e36c91ec354911e8f14e1438c1b8b932b09c160d1738f43310774ead41f3ccf2"
    static let traktClientSecret = "619ef637e74f8532e2950dd0db4977f6cd0debc7745fea1f3826a48ffa71253b"
    static let traktRedirectURI = "mubi://auth"
    static let fanartTvAPIKey = "3d67ed61bea85b0fba45edefd42ebc17"
    
    private struct Domains {
        static let dev = "https://api.trakt.tv"
    }

    private static let BaseURL = Domains.dev
    
    struct fanartTv {
        static let endpointShowImage = "http://webservice.fanart.tv/v3/tv/{showId}"
    }
    
    struct LocalStorage {
        
        static let auth = "auth"
        static let profileSettings = "profileSettings"
        static let watchingShowList = "watchingShowList"
        static let seasonShowList = "seasonShowList"
        static let episodesList = "episodesList"
        static let nextEpisode = "nextEpisode"
        static let progressShow = "progressShow"
    }
    
    struct Auth {
        static let authorize = BaseURL  + "/oauth/authorize"
        static let token = BaseURL + "/oauth/token"
    }
    
    struct Settings {
        static let profileSettings = BaseURL + "/users/settings"
    }
    
    struct Watching {
        static let userWatching = BaseURL + "/sync/watched/{filter}"
        static let progressList = BaseURL + "/shows/{showId}/progress/watched"
    }
    
    struct Seasons {
        static let seasonList = BaseURL + "/shows/{showName}/seasons"
    }
    
    struct Episodes {
        static let episodesList = BaseURL + "/shows/{showId}/seasons/{seasonId}"
        static let nextEpisode = BaseURL + "/shows/{showId}/next_episode"
        static let checkWatched = BaseURL + "/sync/history"
    }
    
}
