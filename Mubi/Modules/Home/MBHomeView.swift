//
//  MBHomeView.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 10/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import UIKit
import Viperit
import PKHUD

//MARK: - Public Interface Protocol
protocol MBHomeViewInterface {
    func refreshTableView(watchingList : [MBShow])
    func showLoader()
    func hideLoader()
}

//MARK: MBHome View
final class MBHomeView: UserInterface {
    
    @IBOutlet weak var tableView: UITableView!
    var watchingList : [MBShow] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        self.tableView.refreshControl?.addTarget(self, action: #selector(MBHomeView.getWatchingList), for: UIControlEvents.valueChanged)
    }
    
}

//MARK: - Public interface
extension MBHomeView: MBHomeViewInterface {

    func getWatchingList(){
        self.presenter.getWatchingList()
    }
    
    func hideLoader() {
        HUD.hide()
    }

    func showLoader() {
        HUD.show(.progress)
    }
    
    func refreshTableView(watchingList : [MBShow]){
        self.watchingList = watchingList
        self.tableView.reloadData()
        self.tableView.refreshControl?.endRefreshing()
    }
}

extension MBHomeView: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return watchingList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.homeTableViewCell.identifier) as! MBHomeTableViewCell
        
        cell.setItem(show: self.watchingList[indexPath.row])
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.showShowDetail(show: self.watchingList[indexPath.row])
    }
}

// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension MBHomeView {
    var presenter: MBHomePresenter {
        return _presenter as! MBHomePresenter
    }
    var displayData: MBHomeDisplayData {
        return _displayData as! MBHomeDisplayData
    }
}
