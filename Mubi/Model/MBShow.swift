//
//  MBShow.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 11/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Foundation
import SwiftyJSON

class MBShow : NSObject, NSCoding {
    
    var id: Int
    var slug: String
    var title: String
    var tvdb: Int
    var bannerImage: String?
    var backgroundImage: String?
    var thumbImage: String?
    
    init(id: Int,
         slug: String,
         title: String,
         tvdb: Int) {
            self.id = id
            self.slug = slug
            self.title = title
            self.tvdb = tvdb
    }
    
    init(id: Int,
         slug: String,
         title: String,
         tvdb: Int,
         bannerImage: String?,
         backgroundImage: String?,
         thumbImage: String?) {
        self.id = id
        self.slug = slug
        self.title = title
        self.tvdb = tvdb
        self.bannerImage = bannerImage
        self.backgroundImage = backgroundImage
        self.thumbImage = thumbImage
    }
    
    init(json : JSON){
        self.id = json["show"]["ids"]["trakt"].intValue
        self.slug = json["show"]["ids"]["slug"].stringValue
        self.title = json["show"]["title"].stringValue
        self.tvdb = json["show"]["ids"]["tvdb"].intValue
    }
    
    required convenience init?(coder decoder: NSCoder) {
        
        guard
            let slug = decoder.decodeObject(forKey: "slug") as? String,
            let title = decoder.decodeObject(forKey: "title") as? String
            else { return nil }
        
            let id = decoder.decodeInteger(forKey: "id")
            let tvdb = decoder.decodeInteger(forKey: "tvdb")
        
        if let bannerImage = decoder.decodeObject(forKey: "bannerImage") as? String,
            let backgroundImage = decoder.decodeObject(forKey: "backgroundImage") as? String,
            let thumbImage = decoder.decodeObject(forKey: "thumbImage") as? String{
            
            self.init(id: id, slug: slug, title: title, tvdb: tvdb, bannerImage: bannerImage, backgroundImage: backgroundImage, thumbImage: thumbImage)
        } else {
            self.init(id: id, slug: slug, title: title, tvdb: tvdb)
        }
        
        
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.id, forKey: "id")
        coder.encode(self.slug, forKey: "slug")
        coder.encode(self.title, forKey: "title")
        coder.encode(self.tvdb, forKey: "tvdb")
        coder.encode(self.bannerImage, forKey: "bannerImage")
        coder.encode(self.backgroundImage, forKey: "backgroundImage")
        coder.encode(self.thumbImage, forKey: "thumbImage")
    }
}
