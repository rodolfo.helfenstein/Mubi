//
//  AppModules.swift
//  Mubi
//
//  Created by Rodolfo Helfenstein Bulgam on 09/05/17.
//  Copyright © 2017 Helfens. All rights reserved.
//

import Viperit

//MARK: - Application modules
enum AppModules: String, ViperitModule {
    case MBWelcome
    case MBHome
    case MBShowDetail
    case MBSeasonDetail
    case MBEpisodeDetail
}
